﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public bool isChanged = false;
    public Sprite changedSprite;

    AudioSource audioData;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Change the sprite when the object is interacted with, increase score
        if (this.GetComponent<SpriteRenderer>().sprite != changedSprite && this.isChanged == true)
        {
            this.GetComponent<SpriteRenderer>().sprite = changedSprite;
            


        audioData = GetComponent<AudioSource>();
        audioData.Play(0);
        Debug.Log("played a sound!");

        Game_Controller_script.instance.score += 1;
        
    }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Mark player as near object
        if (other.tag == "Player")
        {
            print("do it");
            other.GetComponent<PlayerController>().nearObject = true;
            other.GetComponent<PlayerController>().currentObject = this;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //Mark player as away object
        if (other.tag == "Player")
        {
            print("can't do it");
            other.GetComponent<PlayerController>().nearObject = false;
            other.GetComponent<PlayerController>().currentObject = null;
        }
    }
}
