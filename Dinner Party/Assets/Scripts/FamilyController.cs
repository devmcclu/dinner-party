﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FamilyController : MonoBehaviour
{
    //List of all sprites used by Family memeber
    public List<Sprite> spriteDirection;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Change direction of the collider and the sprite based on the direction of the character
    void SwitchDirection()
    {
        int newDirection = Random.Range(0, 3);

        switch (newDirection)
        {
            case 0:
                this.GetComponent<SpriteRenderer>().sprite = spriteDirection[0];
                this.GetComponentInChildren<FamilyTrigger>().ChangeDirection(newDirection);
                break;
            case 1:
                this.GetComponent<SpriteRenderer>().sprite = spriteDirection[1];
                this.GetComponentInChildren<FamilyTrigger>().ChangeDirection(newDirection);
                break;
            case 2:
                this.GetComponent<SpriteRenderer>().sprite = spriteDirection[2];
                this.GetComponentInChildren<FamilyTrigger>().ChangeDirection(newDirection);
                break;
            case 3:
                this.GetComponent<SpriteRenderer>().sprite = spriteDirection[3];
                this.GetComponentInChildren<FamilyTrigger>().ChangeDirection(newDirection);
                break;
        }

    }
}
