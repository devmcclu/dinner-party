﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D body;
    //Movement floats
    float horizontal;
    float vertical;
    //Run speed, editable in engine
    public float runSpeed = 10.0f;
    //Variable for player in sight of Family member
    public bool isCaught = false;
    //Interactable object variables
    public bool nearObject = false;
    public Interactable currentObject;


    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Get movement from WASD/Arror Keys/Left Stick
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        if (Input.GetAxisRaw("Fire1") > 0){
            //Check if near object
            if (this.nearObject == true)
            {
                print("I am edgy");
                this.currentObject.isChanged = true;
                //Check if seen by family member
                if (this.isCaught == true)
                {
                    print("Loser");
                }
            }
        }
    }

    void FixedUpdate()
    {
        //Put movement inputs into usable 2D Vector
        Vector2 movement = new Vector2(horizontal, vertical);
        //Multiply the movement vector by the run speed
        body.velocity = movement.normalized * runSpeed;
    }
}
