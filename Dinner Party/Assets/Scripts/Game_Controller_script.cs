﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Controller_script : MonoBehaviour
{
    public static Game_Controller_script instance;
    public int score;
    public int maxScore;

    void AddScore() {
        score++; 
    }


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (score == maxScore) {
            Fungus.Flowchart.BroadcastFungusMessage ("youwin");
        }

        
    }
}
