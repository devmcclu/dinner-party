﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrannyTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Mark player as near object
        if (other.tag == "Player")
        {
            print("oh no");
            other.GetComponent<PlayerController>().isCaught = true;
            Fungus.Flowchart.BroadcastFungusMessage ("talkgranny");
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //Mark player as away object
        if (other.tag == "Player")
        {
            print("thank god");
            other.GetComponent<PlayerController>().isCaught = false;
        }
    }

    //Change direction of collider
    public void ChangeDirection(int direction)
    {
        switch (direction)
        {
            case 0:
                this.transform.localEulerAngles = new Vector3(0, 0, 270);
                break;
            case 1:
                this.transform.transform.localEulerAngles = new Vector3(0, 0, 270);
                break;
            case 2:
                this.transform.transform.localEulerAngles = new Vector3(0, 0, 180);
                break;
            case 3:
                this.transform.transform.localEulerAngles = new Vector3(0, 0, 90);
                break;
        }
    }
}
